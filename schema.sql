create table questionnaires ( -- or "tests", as the spec likes to call them.
  id serial primary key,
  name text not null
);

-- No additional indexes are required in this (and the next) table because one
-- is created implicitly to support the foreign key constraints. All the queries
-- we run against it are either against the primary key or the implicit indexes.
create table questions (
  id serial primary key,
  on_questionnaire bigint unsigned not null,
  content text not null,
  foreign key (on_questionnaire) references questionnaires (id)
);

-- See note for previous table.
create table answers (
  id serial primary key,
  on_question bigint unsigned not null,
  content text not null,
  is_correct bool not null default false,
  foreign key (on_question) references questions (id)
);

-- This table only exists to prevent session ID conflicts and is effectively
-- write-once and read-heavy. This can be solved much better with e.g. Memcache
-- or Redis or bloom filters or whatever if disk space is precious.
create table sessions (
  session varchar(32) primary key,
  name text not null,
  open_since timestamp not null default current_timestamp
);

-- The read requirements for this table are not specified, however uniqueness
-- is enforced at the session-question tuple level. The only query we currently
-- run against this table is served sufficiently by the primary key index and
-- is optimized to be a LooseScan, if enabled. (Please do enable it!)
create table submissions_answers (
  session varchar(32) not null,
  questionnaire bigint unsigned not null,
  question bigint unsigned not null,
  answer bigint unsigned not null,
  `timestamp` timestamp not null default current_timestamp,
  primary key (session, question),
  foreign key (questionnaire) references questionnaires (id),
  foreign key (question) references questions (id),
  foreign key (answer) references answers (id),
  foreign key (session) references sessions (session)
);

-- This table could be implemented much better as a view.
-- Postgres would totally rule at this.
-- But if you require each submission to be stored as a row, sure, here you go.
-- Although if "normalization is for sissies" [1], this works great for that.
-- It's basically a cache for a tad expensive query over `submissions_answers`
-- to count the correct answers for a given session.
create table submissions (
  session varchar(32) not null,
  submitter_name text not null,
  questionnaire bigint unsigned not null,
  correct_answers integer unsigned not null,
  `timestamp` timestamp not null default current_timestamp,
  primary key (session, questionnaire),
  foreign key (questionnaire) references questionnaires (id),
  foreign key (session) references sessions (session)
);


-- [1]: https://kottke.org/04/10/normalized-data
