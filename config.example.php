<?php

return [
  // Set this to true to print uncaught exceptions in the HTTP response.
  'debug' => false,

  'errors' => [
    // Don't show the tip to enable `debug` on uncaught exceptions.
    // This is helpful if the database settings are wrong, so for the initial
    // config this is false. When you've made sure that the database works,
    // feel free to set this to true.
    'disable_tips' => false,
  ],

  // Database stuff. Should be pretty self-explanatory.
  'db' => [
    // This can be changed to SQLite if you want (lowercase), but the schema
    // isn't adapted for it, and it isn't well tested. Proceed at your own
    // risk.
    'driver' => 'mysql',

    'host' => 'localhost',
    'database' => 'questionnaire',
    'username' => 'webapp',
    'password' => 'hack me',

    // This option is necessary only when using the SQLite driver.
    // The path to the database file.
    // 'file' => '/tmp/discardable.db',
  ],

  'routing' => [
    // The routing mount prefix.
    // If the webapp doesn't live at the root of the web host, this
    // should be changed accordingly; it's used in the routing logic
    // as well as when generating URLs.
    'prefix' => '/',
  ],

  'static' => [
    // Warn when serving static resources via the dynamic stack.
    // This can be done, but is suboptimal because it wastes a lot of resources
    // spinning up PHP, loading the intermediate code etc etc. If you're sure
    // you want to do this, set this to false, but preferrably leave this to
    // the underlying web server which can do this better.
    // If you are using indirect routing, all the static resource URLs will
    // go through index.php, therefore this probably should be set to false
    // to minimize log spam.
    // But it's better to instead take a moment and set up pretty URLs instead.
    'warn' => true,
  ],
];
