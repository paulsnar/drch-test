<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Routes\Questionnaire;

use PN\Questionnaire\{DB, Flash, Session, Template};
use PN\Questionnaire\Web\{CSRF, HTTP};
use PN\Questionnaire\Web\HTTP\{Request, Response};
use PN\Questionnaire\Web\Routing\Route;

use PN\Questionnaire\App\Data\Questionnaires;

class Step extends Route
{
  public $path = '/q/step';

  public function get(Request $rq)
  {
    if (Session::get('q.finishing')) {
      return Response::redirectTo('/q/finish');
    } else if ( ! Session::has('q')) {
      // not currently filling out a questionnaire
      return Response::redirectTo('/');
    }

    $q = Session::get('q.id');
    $step = Session::get('q.step');
    $question_id = Session::get('q.question');

    $q_name = DB::selectOne(
      'select name from questionnaires where id = ?', [ $q ]);
    if ($q_name === null) {
      // the questionnaire has been removed since
      Flash::set('alert', 'Šis tests ir ticis izdzēsts. ' .
        'Atvainojamies par sagādātajām neērtībām.');
      Session::unset('q');
      return Response::redirectTo('/');
    }
    $q_name = $q_name['name'];

    $question = DB::selectOne(
      'select content from questions where id = ?', [ $question_id ]);
    if ($question === null) {
      // that question has been removed -- skip to the next one
      $next = Questionnaires::questionAfter($q, $question_id);
      if ($next === null) {
        // all the answers have been removed since (or the questionnaire itself,
        // in a race condition with the above)
        if (Questionnaires::length($q) === 0) {
          Flash::set('alert', 'Šis tests ir ticis izdzēsts. ' .
            'Atvainojamies par sagādātajām neērtībām.');
          Session::unset('q');
          return Response::redirectTo('/');
        }

        // otherwise, there are still questions but none after this one so
        // apparently this is the last question now (shrug emoji)
        Session::set('q.finishing', true);
        return Response::redirectTo('/q/finish');
      }

      $question_id = $next;
      $question = DB::selectOne(
        'select content from questions where id = ?', [ $question_id ]);
      if ($question === null) {
        // okay this is some serious race conditions here
        Flash::set('alert', 'Atvainojiet, kaut kas nogāja greizi.');
        Session::unset('q');
        return Response::redirectTo('/');
      }
    }

    $answers = DB::query('select id, content from answers ' .
      'where on_question = ?', [ $question_id ]);

    $tpl = new Template('question');
    $tpl['step'] = $step;
    $tpl['total'] = Questionnaires::length($q);
    $tpl['question'] = $question['content'];
    $tpl['answers'] = $answers;

    // skips rendering the scripts
    $tpl['ajax'] = $rq->headers['Ajax'] === '1';

    if ($rq->headers['Ajax'] === '1') {
      return Response::fromTemplate(HTTP::OK, $tpl);
    }

    $layout = new Template('layout.default');
    $tpl->layout = $layout;
    $layout['title'] = $q_name;
    $layout['content'] = $tpl->render();

    return Response::fromTemplate(HTTP::OK, $layout);
  }

  public function post(Request $rq)
  {
    if ($rq->headers['Ajax'] === '1') {
      return $this->postAjax($rq);
    }

    if (Session::has('q.finishing')) {
      return Response::redirectTo('/q/finish');
    } else if ( ! Session::has('q')) {
      return Response::redirectTo('/');
    }

    $csrf = $rq->form['_csrf'];
    if ($csrf === null || ! CSRF::verify($csrf)) {
      Flash::set('alert', 'Kaut kas nogāja greizi. Lūdzu, mēģiniet vēlreiz.');
      return Response::redirectTo('/q/step');
    }

    $q = Session::get('q.id');
    $step = Session::get('q.step');
    $question_id = Session::get('q.question');

    $answer = $rq->form['answer'];
    if ($answer === null) {
      Flash::set('alert', 'Lūdzu, izvēlieties atbildi.');
      return Response::redirectTo('/q/step');
    }

    // The on_question clause in this part ensures that the answer corresponds
    // to the question we are currently asking.
    $correct = DB::selectOne('select is_correct from answers ' .
      'where on_question = ? and id = ?', [ $question_id, $answer ]);
    if ($correct === null) {
      // If not, it fails.
      // Although the error message might be a bit misleading.
      Flash::set('alert', 'Lūdzu, izvēlieties atbildi.');
      return Response::redirectTo('/q/step');
    }

    Session::set('q.step', $step + 1);

    $session = Session::get('user.session');
    $name = Session::get('user.name');

    DB::query('insert into submissions_answers ' .
      '(session, questionnaire, question, answer) values ' .
      '(:session, :questionnaire, :question, :answer)',
      [
        ':session' => $session,
        ':questionnaire' => $q,
        ':question' => $question_id,
        ':answer' => $answer,
      ]);

    $next = Questionnaires::questionAfter($q, $question_id);
    if ($next === null) {
      Session::set('q.finishing', true);
      return Response::redirectTo('/q/finish');
    }

    Session::set('q.question', $next);
    return Response::redirectTo('/q/step');
  }

  protected function postAjax(Request $rq)
  {
    if (Session::has('q.finishing')) {
      return Response::withJson(HTTP::BAD_REQUEST, [ 'error' => 'finishing' ]);
    } else if ( ! Session::has('q')) {
      return Response::withJson(HTTP::BAD_REQUEST, [ 'error' => 'no_session' ]);
    }

    $csrf = $rq->form['_csrf'];
    if ($csrf === null || ! CSRF::verify($csrf)) {
      return Response::withJson(HTTP::BAD_REQUEST, [ 'error' => 'csrf' ]);
    }
    // From here on we can hand out CSRF tokens within the response because
    // the previous one will have been invalidated, but it was valid.

    $q = Session::get('q.id');
    $step = Session::get('q.step');
    $question_id = Session::get('q.question');

    $answer = $rq->form['answer'];
    if ($answer === null) {
      return Response::withJson(HTTP::BAD_REQUEST, [
        'error' => 'missing_field',
        'missing_field' => 'answer',
        'csrf' => CSRF::token(),
      ]);
    }

    $correct = DB::selectOne('select is_correct from answers ' .
      'where on_question = ? and id = ?', [ $question_id, $answer ]);
    if ($correct === null) {
      return Response::withJson(HTTP::BAD_REQUEST, [
        'error' => 'invalid_field',
        'invalid_field' => 'answer',
        'csrf' => CSRF::token(),
      ]);
    }

    Session::set('q.step', $step + 1);

    $session = Session::get('user.session');
    $name = Session::get('user.name');

    DB::query('insert into submissions_answers ' .
      '(session, questionnaire, question, answer) values ' .
      '(:session, :questionnaire, :question, :answer)',
      [
        ':session' => $session,
        ':questionnaire' => $q,
        ':question' => $question_id,
        ':answer' => $answer,
      ]);

    $next = Questionnaires::questionAfter($q, $question_id);
    if ($next === null) {
      Session::set('q.finishing', true);
      return Response::withJson(HTTP::OK, [ 'done' => true ]);
    }

    Session::set('q.question', $next);
    $rq2 = new Request();
    $rq2->headers['Ajax'] = '1';
    return $this->get($rq2);
  }
}
