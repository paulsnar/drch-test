<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Routes\Questionnaire;

use PN\Questionnaire\Utilities\Inflector;
use PN\Questionnaire\{DB, Flash, Session, Template};
use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\HTTP\{Request, Response};
use PN\Questionnaire\Web\Routing\Route;

use PN\Questionnaire\App\Data\{Questionnaires, Sessions};

class Finish extends Route
{
  public $path = '/q/finish';

  public function get(Request $rq)
  {
    if ( ! Session::has('q')) {
      return Response::redirectTo('/');
    } else if ( ! Session::get('q.finishing')) {
      return Response::redirectTo('/q/step');
    }

    $q = Session::get('q.id');
    $step = Session::get('q.step');

    $session = Session::get('user.session');
    $name = Session::get('user.name');

    if ($q === null) {
      Session::unset('q');
      return Response::redirectTo('/');
    }

    $total = Questionnaires::length($q);
    if ($total === 0) {
      // hm
      Flash::set('alert', 'Šis tests ir ticis iztukšots. ' .
        'Atvainojamies par sagādātajām neērtībām.');
      Session::unset('q');
      return Response::redirectTo('/');
    }

    if ($step < $total) {
      // not finished yet
      return Response::redirectTo('/q/step');
    }
    if ($step > $total) {
      // prevent odd numbers (4 out of 3) in case the questionnaire was changed
      // during filling out
      $step = $total;
    }

    $correct = Sessions::countCorrect($session);
    DB::query('insert into submissions ' .
      '(session, submitter_name, questionnaire, correct_answers) values ' .
      '(:session, :name, :questionnaire, :correct)',
      [
        ':session' => $session,
        ':name' => $name,
        ':questionnaire' => $q,
        ':correct' => $correct,
      ]);

    $tpl = new Template('finish');
    $tpl['name'] = Inflector::vocative($name);
    $tpl['correct'] = $correct;
    $tpl['total'] = $total;

    Session::unset('q');
    Session::unset('user.session'); // we can keep the name

    return Response::fromTemplate(HTTP::OK, $tpl);
  }
}
