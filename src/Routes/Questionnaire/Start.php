<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Routes\Questionnaire;

use PN\Questionnaire\{DB, Flash, Log, Session};
use PN\Questionnaire\Web\{CSRF, HTTP};
use PN\Questionnaire\Web\HTTP\{Request, Response};
use PN\Questionnaire\Web\Routing\Route;

use PN\Questionnaire\App\Data\{Questionnaires, Sessions};

class Start extends Route
{
  public $path = '/q/start';

  public function get(Request $rq)
  {
    if (Session::get('q.finishing')) {
      return Response::redirectTo('/q/finish');
    } else if (Session::has('q')) {
      return Response::redirectTo('/q/step');
    } else {
      return Response::redirectTo('/');
    }
  }

  public function post(Request $rq)
  {
    if (Session::get('q.finishing')) {
      return Response::redirectTo('/q/finish');
    } else if (Session::has('q')) {
      return Response::redirectTo('/q/step');
    }

    $csrf = $rq->form['_csrf'];
    if ($csrf === null || ! CSRF::verify($csrf)) {
      Flash::set('alert', 'Kaut kas nogāja greizi. Lūdzu, mēģiniet vēlreiz.');
      return Response::redirectTo('/');
    }

    $name = $rq->form['name'];
    if ($name === null || trim($name) === '') {
      Flash::set('alert', 'Lūdzu, norādiet savu vārdu.');
      return Response::redirectTo('/');
    }
    Flash::set('frontpage.form.name', $name);

    $q = $rq->form['q'];
    if ($q === null || ! ctype_digit($q)) {
      Flash::set('alert', 'Lūdzu, izvēlieties testu.');
      return Response::redirectTo('/');
    }

    $q_ok = DB::selectOne('select count(1) as ok from questionnaires ' .
      'where id = ?', [ $q ])['ok'];
    if ($q_ok !== '1') {
      Flash::set('alert', 'Lūdzu, izvēlieties testu.');
      return Response::redirectTo('/');
    }

    $question = Questionnaires::questionAfter($q, 0);
    if ($question === null) {
      // empty questionnaire?
      Log::warning('questionnaire %d is empty', $q);
      Flash::set('alert',
        'Šis tests ir tukšs. Lūdzu, izvēlieties citu testu.');
      return Response::redirectTo('/');
    }

    $session = Sessions::uniqueID();
    DB::query('insert into sessions (session, name) values (?, ?)',
      [ $session, $name ]);

    Session::set('user', [
      'session' => $session,
      'name' => $name,
    ]);
    Session::set('q', [
      'id' => $q,
      'step' => 1,
      'question' => $question,
    ]);

    Flash::clear();
    return Response::redirectTo('/q/step');
  }
}
