<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Routes;

use PN\Questionnaire\{DB, Flash, Session, Template};
use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\HTTP\{Request, Response};
use PN\Questionnaire\Web\Routing\Route;

class Frontpage extends Route
{
  public $path = '/';

  public function get(Request $rq)
  {
    $tpl = new Template('frontpage');
    $tpl['qs'] = DB::query('select id, name from questionnaires');

    if (Session::has('user.name')) {
      $name = Session::get('user.name');
      Session::unset('user');
      Flash::set('frontpage.form.name', $name);
    }

    if (Session::has('q')) {
      Session::unset('q');
    }

    return Response::fromTemplate(HTTP::OK, $tpl);
  }
}
