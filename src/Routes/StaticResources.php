<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Routes;

use function PN\Questionnaire\path_join;
use PN\Questionnaire\{Config, Log};
use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\HTTP\{Request, Response};
use PN\Questionnaire\Web\Routing\{DefaultNotFoundRoute, Route};

/**
 * A fallback for serving static resources.
 *
 * This is less efficient than doing this within your web server, so a warning
 * is printed each time this route is invoked. If you're sure you want to do
 * this, set the config option static.warn to false.
 */
class StaticResources extends Route
{
  public $path = '/static/', $exact = false;

  const MIMETYPES = [
    'css' => 'text/css',
  ];

  public function get(Request $rq)
  {
    $path = substr($rq->path, strlen('/static/'));
    $fspath = path_join(Q_PUBLIC_ROOT, 'static', $path);

    if (file_exists($fspath)) {
      if (Config::get('static.warn') !== false) {
        Log::warning("serving static resource {$path} via PHP");
      }
      $last_dot = strrpos($fspath, '.');
      $extension = substr($fspath, $last_dot + 1);

      $mimetype = static::MIMETYPES[$extension] ?? 'application/octet-stream';
      if (strpos($mimetype, 'text/') === 0) {
        $mimetype .= '; charset=UTF-8'; // TODO: bad assumption?
      }

      return new Response(HTTP::OK, [
        'Content-Type' => $mimetype,
      ], file_get_contents($fspath));
    }

    return new Response(HTTP::NOT_FOUND, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ], DefaultNotFoundRoute::DEFAULT_ERROR_MESSAGE);
  }
}
