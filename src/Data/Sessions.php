<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Data;

use PN\Questionnaire\DB;

abstract class Sessions
{
  /**
   * Returns a (probably) unique ID that isn't currently in use by any
   * other session, using the database as the source of truth.
   *
   * @return string
   */
  public static function uniqueID()
  {
    $exists = true;
    do {
      $id = bin2hex(random_bytes(16));
      $exists = DB::selectOne('select count(1) as e from sessions ' .
        'where session = ?', [ $id ])['e'] === '1';
    } while ($exists);
    return $id;
  }

  /**
   * Returns the current count of correct answers for the given session.
   *
   * @param string|int $id
   * @return int
   */
  public static function countCorrect($id)
  {
    $r = DB::selectOne('select count(1) as correct from answers ' .
      'where is_correct = true and id in (' .
        'select answer from submissions_answers where session = ?)', [ $id ]);
    return intval($r['correct']);
  }
}
