<?php declare(strict_types=1);

namespace PN\Questionnaire\App\Data;

use PN\Questionnaire\DB;

abstract class Questionnaires
{
  /**
   * Fetches the length of the given questionnaire, as measured in questions.
   *
   * @param string|int $id
   * @return int
   */
  public static function length($id)
  {
    $len = DB::selectOne('select count(1) as length from questions ' .
      'where on_questionnaire = ?', [ $id ]);
    if ($len === null) {
      throw new \Exception("Bad request for questionnaire {$id}");
    }
    return intval($len['length']);
  }

  /**
   * Fetches the ID of the question following the specified one
   * in the given questionnaire.
   *
   * Pass 0 as the question to obtain the ID of the first question.
   *
   * @param string|int $questionnaire
   * @param string|int $question
   * @return string
   */
  public static function questionAfter($questionnaire, $question)
  {
    $q = DB::selectOne('select id from questions ' .
      'where on_questionnaire = ? and id > ?', [ $questionnaire, $question ]);
    if ($q === null) {
      return null;
    }
    return $q['id'];
  }
}
