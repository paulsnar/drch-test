<?php declare(strict_types=1);

namespace PN\Questionnaire\App;

use PN\Questionnaire\Web\Routing\Router;

abstract class Routes
{
  public const ROUTES = [
    Routes\StaticResources::class,
    Routes\Frontpage::class,
    Routes\Questionnaire\Start::class,
    Routes\Questionnaire\Step::class,
    Routes\Questionnaire\Finish::class,
  ];

  public static function register()
  {
    foreach (static::ROUTES as $route) {
      // A great opportunity for dependency injection!
      $inst = new $route();
      Router::register($inst);
    }
  }
}
