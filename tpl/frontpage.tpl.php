---
layout = layout.default
---
<div class="narrow">
  <h1>Testa uzdevums</h1>
  <?php if (Flash::has('alert')) { ?>
  <div class="alert">
    <?= Flash::get('alert') ?>
  </div>
  <?php } ?>
  <form action="<?= url('/q/start') ?>" method="POST" id="js-form">
    <input type="hidden" name="_csrf" value="<?= csrf() ?>">
    <input type="text" name="name" id="js-form-name" class="form-el"
      <?php if (Flash::has('frontpage.form.name')) { ?>
      value="<?= he(Flash::get('frontpage.form.name')) ?>" <?php } ?>
      placeholder="Ievadi savu vārdu">
    <select name="q" class="form-el" id="js-form-q">
      <option value="" selected disabled>Izvēlies testu</option>
      <?php foreach ($qs as $q) { ?>
      <option value="<?= he($q['id']) ?>"><?= he($q['name']) ?></option>
      <?php } ?>
    </select>
    <button type="submit">Sākt</button>
  </form>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var $form = document.getElementById('js-form'),
          $name = document.getElementById('js-form-name'),
          $q = document.getElementById('js-form-q');

      $form.addEventListener('submit', function(ev) {
        if ($name.value.trim() === '') {
          alert('Lūdzu, norādiet savu vārdu.');
          ev.preventDefault();
          $name.focus();
          return false;
        }

        if ($q.value === '') {
          alert('Lūdzu, izvēlieties testu.');
          ev.preventDefault();
          $q.focus();
          return false;
        }
      });
    });
  </script>
</div>
