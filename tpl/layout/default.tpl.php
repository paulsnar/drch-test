<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>
  <?= ($title ?? false) ? "{$title} :: Questionnaire" : "Questionnaire" ?>
</title>
<link rel="stylesheet" href="<?= url('/static/style.css') ?>">
<?= $content ?>
