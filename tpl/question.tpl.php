<div id="content">
  <h1><?= $question ?></h1>
  <?php if (Flash::has('alert')) { ?>
  <div class="alert">
    <?= Flash::get('alert') ?>
  </div>
  <?php } ?>
  <form action="<?= url('/q/step') ?>" method="POST" id="js-form">
    <input type="hidden" name="_csrf" id="_csrf" value="<?= csrf() ?>">
    <div class="form-options">
      <?php foreach ($answers as $answer) { ?>
      <label class="form-option">
        <input type="radio" name="answer" value="<?= $answer['id'] ?>">
        <?= he($answer['content']) ?>
      </label>
      <?php } ?>
    </div>
    <button type="submit" id="js-submit-button">Nākamais</button>
    <div class="progress-bar">
      <div class="progress-bar-value"
          style="width: <?= (100 * $step / $total) - 5 ?>%">
        <?= round(100 * $step / $total) ?>%
      </div>
    </div>
  </form>
</div>
<?php if ( ! $ajax) { ?>
<script>
  document.addEventListener('DOMContentLoaded', function rebind() {
    var reduce = Array.prototype.reduce.call.bind(Array.prototype.reduce);

    var $labels = document.getElementsByTagName('label'),
        $radios = document.querySelectorAll('input[type="radio"]');
    for (var i = 0, l = $radios.length; i < l; i += 1) {
      $radios[i].addEventListener('change', _handleRadioChanged);
    }
    function _handleRadioChanged(ev) {
      for (var i = 0, l = $labels.length; i < l; i += 1) {
        $labels[i].classList.remove('--active');
      }
      ev.target.parentElement.classList.add('--active');
    }

    var $form = document.getElementById('js-form'),
        $submit = document.getElementById('js-submit-button');
    var submitting = false;
    $form.addEventListener('submit', function(ev) {
      if (submitting) { return; }

      var $radios = $form.querySelectorAll(
        'input[type="radio"][name="answer"]');
      var checked = reduce($radios, function(accum, radio) {
        return radio.checked ? radio.value : accum;
      }, null);
      if (checked === null) {
        alert('Lūdzu, izvēlieties atbildi.');
        ev.preventDefault();
        return false;
      }

      submitting = true;
      $submit.disabled = true;
      $submit.textContent = 'Lūdzu uzgaidiet...';

      var formData = new FormData();
      formData.append('_csrf', document.getElementById('_csrf').value);
      formData.append('answer', checked);

      fetch(<?= json(url('/q/step')) ?>, {
        method: 'POST',
        headers: { Ajax: '1' },
        body: formData,
        credentials: 'same-origin',
      }).then(function(resp) {
        if (resp.headers.get('Content-Type').startsWith('text/html')) {
          resp.text().then(function(html) {
            document.getElementById('content').innerHTML = html;
            // clean up to prevent memory leaks
            for (var i = 0, l = $radios.length; i < l; i += 1) {
              $radios[i].removeEventListener('change', _handleRadioChanged);
            }
            // redo the script
            rebind();
          });
        } else {
          resp.json().then(function(jsonData) {
            if ('error' in jsonData) {
              alert('Notika kļūme. Lūdzu, mēģiniet vēlreiz.');
              window.location.reload();
            } else if (jsonData.done) {
              window.location.href = <?= json(url('/q/finish')) ?>;
            }
          }, function(err) { /* noop */ });
        }
      });

      ev.preventDefault();
    });
  });
</script>
<?php } ?>
