insert into questionnaires (name) values ('quest 1');
set @q = last_insert_id();

insert into questions (on_questionnaire, content) values (@q, 'q1');
set @q1 = last_insert_id();
insert into answers (on_question, content, is_correct)
  values (@q1, 'a1', false), (@q1, 'a2+', true);

insert into questions (on_questionnaire, content) values (@q, 'q2');
set @q2 = last_insert_id();
insert into answers (on_question, content, is_correct)
  values
    (@q2, 'a1', false), (@q2, 'a2', false), (@q2, 'a3', false),
    (@q2, 'a4+', true), (@q2, 'a5', false), (@q2, 'a6', false),
    (@q2, 'a7', false), (@q2, 'a8', false), (@q2, 'a9', false),
    (@q2, 'a10', false), (@q2, 'a11', false);

insert into questions (on_questionnaire, content) values (@q, 'q3');
set @q3 = last_insert_id();
insert into answers (on_question, content, is_correct)
  values (@q3, 'a1+', true), (@q3, 'a2', false);
