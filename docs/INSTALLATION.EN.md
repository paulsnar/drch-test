# Setup

## Technical requirements

At the present this application has been tested to be compatible with:

* Unix-type operating systems (macOS and Linux in particular)
* PHP 7.2.8 with the PDO-MySQL extension
* MariaDB 10.3.8
* Nginx 1.14.0 with PHP via PHP-FPM

Running the application is technically possible, albeit not tested, with:

* other Unix-type operating systems (BSD, Solaris/illumos etc.), Windows
* PHP 7.2 or newer
* MariaDB 10 and newer
* other web servers and older Nginx versions

## Setup procedure (TL;DR)

 1. Create a new database user and a database. Insert the tables defined by
    `schema.sql` as well as the necessary data (a sample is given in
    `mock-data.sql`).
 1. Put the application into a folder (hereinafter -- the project root).
 1. Copy `config.example.php` to `config.php`; replace the settings within the
    `db` array with ones matching the results of the 1st step.
 1. Set the `public` directory as the web root or create a symbolic link within
    the web root pointing to the `public` directory in the project root. If
    neither's possible, the `public` directory can also be copied, with the
    condition that the 17th line of `index.php` must be adjusted by replacing
    `dirname(__DIR__)` with the absolute path of the project root.
 1. If the `public` directory isn't the web root, then its URL must be
    specified as the `routing.prefix` property in the configuration file.
 1. For proper routing pretty URLs must be set up -- see below.
 1. If everything has been performed correctly, upon visiting the application
    you should be greeted by the 1st view (the "homepage" -- offers the choice
    of tests and a field to enter your name in). If not, feel free to contact
    me via the means provided on [my website](https://pn.id.lv).

## Setting up pretty URLs

During development this application was served by the PHP builtin server which
offers pretty URLs automatically but is pretty inefficient when it comes to
running at scale. For development or testing purposes it can be run like this,
when your working directory is the `public` directory of the project root:

    $ php -t . -S 0.0.0.0:8080 index.php

after which it will be listening on port 8080.

The application has also been tested with Nginx and PHP-FPM on Arch Linux
(versions 1.14.0 and 7.2.8 respectively). Configuring Nginx in general is a bit
more difficult, however the part concerned with pretty URLs is the following,
which should be inserted within the `server` block:

    location /a/b/c/ {
      try_files $uri /a/b/c/index.php;
    }

(*Of course, replace `/a/b/c/` with the location of the `public` directory
within your web root. If the `public` directory is the web root, use `/`.*)

In the case that neither solution is available, the application also supports
indirect routing, that is, using `/a/b/c/index.php/` as the pretty URL root
(i.e. transforming the pretty URL `/q/step` into `/a/b/c/index.php/q/step`).
If your web server supports such an option, it's available and supported,
setting `routing.prefix` accordingly (which means including `index.php` as part
of it).

Apache requires an `.htaccess` file. An example is provided within the `public`
directory, albeit it has not been tested.

Note that the configuration option `routing.prefix` must be adjusted to reflect
the location of the `public` directory within your web root (and include
`index.php` if using indirect routing, as mentioned above), or else neither
routing nor generating URLs will work and the application won't be usable.
