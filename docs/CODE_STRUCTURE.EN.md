Some notes on the code layout.

If you look into the project root you'll be greeted by a couple of directories:

* `docs` contains the documentation you're reading right now (hi!)
* `lib` contains generic support library code. It's intended to be highly
  reusable and forms a makeshift framework.
* `public` is the public root. It's covered extensively in [the installation
  documentation](./INSTALLATION.EN.md).
* `src` contains specialized application code. That's mostly data definition
  and route handlers.
* `tpl` contains templates, used for view generation.
* `config.example.php` contains the configuration example, and if the app has
  already been installed, also `config.php`.
* `mock-data.sql` and `schema.sql` contains SQL mock data and schema,
  respectively. They can be considered MySQL dump files if you really want, but
  really they're just plain SQL.

If you think about it, there's a sort of MVC pattern going on -- the route
handlers (`src/Routes`) form the controllers, the data lookups (`src/Data`) are
parts of the model, and the templates (`tpl`) are the view. It's a bit
unconventional because it doesn't call that fact out, but it's there all right.
