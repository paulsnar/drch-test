Some context on the database layout and semantics.

## Structure

* The table `questionnaires` stores information about each ongoing "test"
  (hereinafter we shall refer to "tests" as questionnaires because that's
  what they are).
* The table `questions` stores all questions currently present. Each of them
  has a parent questionnaire, denoted as a foreign key reference into the
  `questionnaires` table, and the body of the question itself.
* The table `answers` stores all answer options to questions currently present.
  Each of them has a parent question, denoted as a foreign key reference into
  the `questions` table, the body of the answer itself, and a flag whether this
  answer is the correct one for the given question.

Note that there can be more than one correct answer to a question. This isn't
strictly listed in the requirements, but as it stands now it has only
unidirectional data dependencies (the questions table doesn't reference answers
because that would break the parent-child relationship) and also allows for
future expandability when multiple correct answers would be needed for a
question. No code changes are currently required to handle that case.

The table `sessions` stores currently ongoing or previously finished sessions,
where one is understood as a client currently filling out a questionnaire or
having done so in the past. This is mostly stored for reference.

The table `submissions_answers` stores submitted answers to particular
questions by particular sessions (requirement Datubāze/5.). It's pretty
self-explanatory in my opinion -- it stores foreign key references to the
specific session, question and answer. I consider the questionnaire reference
to be redundant because that can be deduced via the questions table, but it is
present to be compliant with the requirement.

The table `submissions` stores finished session results (requirement
Datubāze/6.). Note that the submitter name is pulled out from the sessions
table, and the `correct_answers` column stores seemingly redundant information.
This is because this table is optimized to be partitioned away on a separate
master or even unrelated server. This is in order to have "high scalability"
of the database. The foreign key references could well be removed if necessary,
because referential integrity is maintained in the application layer.

## Indexes

Note that no explicit indexes are defined. This is intentional, because the
foreign key constraints define implicit indexes which are then used in all the
queries currently run against the database.

Most of the queries are either primary key lookups or explicitly required full
table scans (e.g. for the frontpage to list active questionnaires), so I won't
list those. Here are some others which might seem a bit expensive but are
optimized:

Upon every request the total question count in the given questionnaire is
calculated, using the sql query `select count(1) as length from questions
where on_questionnaire = ?`. This uses only the implicit index defined as
part of the foreign key relation; the EXPLAIN result is as follows:

```
           id: 1
  select_type: SIMPLE
        table: questions
         type: ref
possible_keys: on_questionnaire
          key: on_questionnaire
      key_len: 8
          ref: const
         rows: 3
          Extra: Using index
```

Upon finishing the questionnaire a query to count the correct answers is run:
`select count(1) as correct from answers where is_correct = true and id in
(select answer from submissions_answers where session = ?)`. This is a tad
less efficient (and the reason that `submissions` stores the count of correct
questions), however it still can be considered optimal:

```
*************************** 1. row ***************************
           id: 1
  select_type: PRIMARY
        table: submissions_answers
         type: index
possible_keys: PRIMARY,answer
          key: answer
      key_len: 10
          ref: NULL
         rows: 12
        Extra: Using where; Using index; LooseScan

*************************** 2. row ***************************
           id: 1
  select_type: PRIMARY
        table: answers
         type: eq_ref
possible_keys: PRIMARY
          key: PRIMARY
      key_len: 8
          ref: questionnaire.submissions_answers.answer
         rows: 1
        Extra: Using where
```

As you can see, it's mostly a loose index scan followed by an eq_ref lookup.

Therefore I concur that my choice of no additional indexes saves disk space by
not storing unnecessary duplicate data yet doesn't adversely affect any queries
that are currently performed.
