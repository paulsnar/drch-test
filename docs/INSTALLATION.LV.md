# Uzstādīšana

## Tehniskās prasības

Patlaban šī lietotne ir savietojama ar:

* Unix tipa operētājsistēmām (precīzāk, macOS un Linux)
* PHP 7.2.8 ar PDO-MySQL paplašinājumu
* MariaDB 10.3.8
* Nginx 1.14.0 (PHP nodrošināts caur PHP-FPM)

Lietotnes darbināšana principā ir iespējama, taču nav pārbaudīta, ar:

* citām Unix tipa operētājsistēmām (BSD, Solaris/illumos utt.), Windows
* PHP 7.2 vai jaunāku
* MariaDB 10 un jaunāku
* citiem tīmekļa serveriem un vecākām Nginx versijām

## Uzstādīšanas procedūra (TL;DR)

 1. Izveidojiet jaunu datubāzi un asociēto lietotāju. Izvietojiet failā
    `schema.sql` norādītās tabulas un nepieciešamos datus (paraudziņš ir dots
    failā `mock-data.sql`).
 1. Izvietojiet lietotnes failus mapē (turpmāk -- projekta sakne).
 1. Kopējiet `config.example.php` uz `config.php`; nomainiet `db` masīvā esošos
    parametrus atbilstīgi 1. solī noteiktajiem.
 1. Izvietojiet `public` mapi kā tīmekļa servera sakni, vai izvietojiet
    simbolisko saiti iekš tīmekļa servera saknes, norādošu uz `public` mapi. Ja
    tas nav iespējams, visa `public` mape var tikt kopēta, ņemot vērā, ka
    `index.php` faila 17. rindiņā `dirname(__DIR__)` jāaizvieto ar projekta
    saknes absolūto failu sistēmas lokāciju.
 1. Ja `public` mape netika izvietota kā tīmekļa servera saknke, konfigurācijas
    parametrs `routing.prefix` jāuzstāda kā `public` mapes tīmekļa vietrādis.
 1. Pareizai maršrutēšanai nepieciešams uzstādīt "daiļos vietrāžus" --
    instrukcijas dotas zemāk.
 1. Ja visi soļi ir izpildīti pareizi, apmeklējot lietotni tīmekļa pārlūkā Jūs
    sagaida 1. skats ("sākumlapa" -- piedāvā izvēlēties testu un ievadīt Jūsu
    vārdu). Ja tas tā nav, lūdzu, sazinieties ar mani, izmantojot [manā
    mājaslapā norādītās metodes](https://pn.id.lv).

## "Daiļo vietrāžu" uzstādīšana

Lietotnes izstrādes laikā tika izmantots PHP iebūvētais serveris. Tas atbalsta
daiļos vietrāžus un ir vienkārši konfigurējams un piemērots testēšanai vai
izstrādei, taču nevar tikt uzskatīts par optimālu darbināšanai lielā mērogā.
Lai to palaistu, no mapes `public` komandrindiņā jāpalaiž šis:

    $ php -t . -S 0.0.0.0:8080 index.php

Pēc dotās komandas izpildes serveris uzklausa pieprasījumus caur 8080. portu.

Lietotne ir arī tapusi pārbaudīta, izmantojot Nginx un PHP-FPM uz Arch Linux
distributīva (attiecīgi ar versijām 1.14.0 un 7.2.8). Nginx konfigurācija kopumā
ir nedaudz sarežģītāka, taču daiļo vietrāžu uzstādīšanai Nginx konfigurācijas
failos `server` blokā jāiekļauj sekojošais:

    location /a/b/c/ {
      try_files $uri /a/b/c/index.php;
    }

(*Protams, neaizmirstiet aizvietot `/a/b/c/` ar `public` mapes vietrādi Jūsu
tīmekļa serverī. Ja `public` mape ir tīmekļa sakne, lietojiet `/`.*)

Gadījumā, ja neviens no šiem variantiem nav pieejams, lietotne arī atbalsta
netiešo maršrutēšanu, proti, izmantojot `/a/b/c/index.php/` kā daiļo vietrāžu
sakni (t.i. pārveidojot daiļo vietrādi `/q/step` par `/a/b/c/index.php/q/step`).
Ja Jūsu lietotais tīmekļa serveris atbalsta šādu opciju, tā ir pieejama un tiek
atbalstīta, ja konfigurācijas opcija `routing.prefix` tiek attiecīgi uzstādīta
(iekļaujot `index.php` kā daļu no maršrutēšanas prefiksa).

Apache ir nepieciešams `.htaccess` fails. Distributīvā ir iekļauts paraugs, kas
atrodams `public` mapē, taču tas nav pārbaudīts.

Ņemiet vērā, ka konfigurācijas opcijai `routing.prefix` ir jāatspoguļo `public`
mapes vietrādis (un jāiekļauj `index.php`, ja tiek izmantota netiešā
maršrutēšana kā aprakstīts iepriekš), citādi maršrutēšana un vietrāžu ģenerācija
nedarbosies un lietotne nebūs izmantojama.
