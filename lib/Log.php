<?php declare(strict_types=1);

namespace PN\Questionnaire;

/**
 * A very basic implementation of message logging.
 */
abstract class Log
{
  /**
   * Log a given message at the `error` level.
   *
   * Accepts `printf`-like arguments.
   *
   * @param string $msg
   * @param mixed ...$args
   */
  public static function error($msg, ...$args)
  {
    $msg = sprintf($msg, ...$args);
    if (\PHP_SAPI === 'cli-server') {
      file_put_contents('php://stderr', "[error] {$msg} \n");
    } else {
      syslog(\LOG_ERR, $msg);
    }
  }

  /**
   * Log a given message at the `warning` level.
   *
   * Accepts `printf`-like arguments.
   *
   * @param string $msg
   * @param mixed ...$args
   */
  public static function warning($msg, ...$args)
  {
    $msg = sprintf($msg, ...$args);
    if (\PHP_SAPI === 'cli-server') {
      file_put_contents('php://stderr', "[warning] {$msg}\n");
    } else {
      syslog(\LOG_WARNING, $msg);
    }
  }
}
