<?php declare(strict_types=1);

namespace PN\Questionnaire\DB;

class ConnectionFailure extends \Exception
{
  public function __construct(\Throwable $prev)
  {
    $msg = $prev->getMessage();
    parent::__construct("Could not connect to the database: {$msg}", 0, $prev);
  }
}
