<?php declare(strict_types=1);

namespace PN\Questionnaire\DB;

class PDOException extends \Exception
{
  /**
   * Creates a new PDOException from the PDO errorstate.
   *
   * Example: `new PDOException($db->errorInfo());`
   *
   * @param array $errorInfo
   */
  public function __construct(array $errorInfo) {
    [ $sqlstate, $code, $msg ] = $errorInfo;

    parent::__construct("{$msg} (SQLSTATE {$sqlstate})", $code);
  }
}
