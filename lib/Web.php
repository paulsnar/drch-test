<?php declare(strict_types=1);

namespace PN\Questionnaire;

use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\HTTP\{Request, Response};
use PN\Questionnaire\Web\Routing\Router;

use PN\Questionnaire\App\Routes;

abstract class Web
{
  const DEFAULT_HTML_ERROR_MESSAGE = "<!DOCTYPE html>\n" .
    "<article>Sorry, something went wrong.</article>\n";

  /**
   * Create a request from the global state, map it onto a response and send it.
   */
  public static function dispatch()
  {
    ob_start();

    Routes::register();

    $request = Request::fromGlobals();

    $route = Router::dispatch($request);
    $method = strtolower($request->method);

    if (method_exists($route, 'any')) {
      $response = $route->any($request);
    } else if (method_exists($route, $method)) {
      $response = $route->$method($request);
    } else {
      // hmm
      Log::warning('route %s cannot handle %s, returning 405',
        get_class($route), $request->method);
      $response = new Response(HTTP::METHOD_NOT_ALLOWED, [
        'Content-Type' => 'text/html; charset=UTF-8',
      ], static::DEFAULT_HTML_ERROR_MESSAGE);
    }

    $response->send();
    if (ob_get_level() > 0) {
      ob_end_flush();
    }
  }
}
