<?php declare(strict_types=1);

namespace PN\Questionnaire\Utilities;

abstract class Inflector
{
  /**
   * Transforms a given person's name (given in nominative) into its vocative.
   *
   * Assumes that no personal names exist in the 6th declension.
   */
  public static function vocative($name)
  {
    if (strrpos($name, 's') === strlen($name) - 1 &&
        strrpos($name, 'us') !== strlen($name) - 2) {
      // 1st declension (Artūrs -> Artūr)
      // 2nd declension (Andris -> Andri)
      return substr($name, 0, -1);
    } else {
      // 3rd declension (Mikus -> Mikus) (or is it Miku?)
      // 4th declension (Anna)
      // 5th declension (Alise)
      // 6th declension (??? couldn't think of any)
      // others (Oto)
      return $name;
    }
  }
}
