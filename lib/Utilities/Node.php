<?php declare(strict_types=1);

namespace PN\Questionnaire\Utilities;

/**
 * A wrapper for an object.
 *
 * Of no use in particular by itself, but can be used within other structures.
 */
class Node
{
  public $value;

  public function __construct($value) { $this->value = $value; }
}
