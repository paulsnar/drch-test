<?php declare(strict_types=1);

namespace PN\Questionnaire\Utilities;

/**
 * A radix tree which returns an earlier matched prefix, if any.
 *
 * Great for route mounting.
 */
class PrefixTree extends RadixTree
{
  /** @inheritdoc */
  public function lookup($key)
  {
    $node =& $this->root;
    $parent = null;
    $lastEmpty = null;

    while (true) {
      if ($node instanceof Node) {
        return $node->value;
      }

      $keylen = strlen($key);

      foreach ($node as $prefix => $value) {
        if ($prefix === $key) {
          $parent = $node;
          $node = $value;
          if ($node instanceof Node) {
            return $node->value;
          } else if (array_key_exists('', $node)) {
            return $node['']->value;
          }
          return $lastEmpty;
        }

        $preflen = strlen($prefix);

        if ($preflen === 0) {
          $lastEmpty = $value->value;
          continue 1;
        }

        if ($keylen > $preflen &&
            substr($key, 0, $preflen) === $prefix) {
          // prefix match
          $key = substr($key, $preflen);
          $parent = $node;
          $node = $value;
          continue 2;
        }
      }

      return $lastEmpty;
    }
  }
}
