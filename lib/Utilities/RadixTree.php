<?php declare(strict_types=1);

namespace PN\Questionnaire\Utilities;

/**
 * A radix tree implementation.
 *
 * @see https://en.wikipedia.org/wiki/Radix_tree
 */
class RadixTree
{
  protected $root;

  /**
   * Create a new radix tree.
   *
   * @param array $initial Key-value set of values to be inserted.
   */
  public function __construct(array $initial = [ ])
  {
    $this->root = [ ];
    foreach ($initial as $key => $value) {
      $this->insert($key, $value);
    }
  }

  /**
   * Insert a new node into the radix tree, or replace an existing one.
   *
   * @param string $key
   * @param mixed $value
   */
  public function insert($key, $value)
  {
    $value = new Node($value);

    // If the root is empty, no need to traverse it
    if (count($this->root) === 0) {
      $this->root[$key] = $value;
      return;
    }

    $_key = $key; // save original key for use in exceptions
    $node =& $this->root;
    while (true) {
      $keylen = strlen($key);

      foreach ($node as $prefix => &$subnode) {
        if ($prefix === $key) {
          if ($subnode instanceof Node) {
            $subnode = $value;
            return;
          }
          $subnode[''] = $value;
          return;
        }

        $preflen = strlen($prefix);

        if ($preflen === 0) {
          // skip empty prefixes, because they can't contain subnodes
          // and will just confuse the rest of the code
          continue 1;
        }

        if ($keylen > $preflen &&
            substr($key, 0, $preflen) === $prefix) {
          // current prefix matches the key, but key is longer (e.g. has suffix)
          if ($subnode instanceof Node) {
            // the node is a value
            $subnode = [ '' => $subnode ];
            $subnode[substr($key, $preflen)] = $value;
            return;
          } else {
            // the node is a leaf
            $node =& $subnode;
            // continue with prefix stripped
            $key = substr($key, $preflen);
            continue 2; // traverse further
          }
        }

        if ($preflen > $keylen &&
            substr($prefix, 0, $keylen) === $key) {
          // current prefix matches the key, but key is shorter
          $suffix = substr($prefix, $keylen);
          unset($node[$prefix]);
          $node[$key] = [ $suffix => $subnode, '' => $value ];
          return;
        }

        // Search for partial common prefix (e.g. shorter than both the key
        // and the current leaf)
        $common = $keylen > $preflen ? $prefix : $key;
        $other = $keylen > $preflen ? $key : $prefix;
        while (strlen($common) > 0) {
          if (strpos($other, $common) === 0) {
            break;
          }
          $common = substr($common, 0, -1);
        }
        if (strlen($common) === 0) {
          // no common prefix, try next leaf
          continue 1;
        }
        $commlen = strlen($common);

        unset($node[$prefix]);
        $prefix = substr($prefix, $commlen);
        $key = substr($key, $commlen);
        $node[$common] = [ $prefix => $subnode, $key => $value ];
        return;
      }

      // we've gone through all keys and haven't found anything common
      // so we put this value right here
      $node[$key] = $value;
      return;
    }
  }

  /**
   * Look a node up by its key.
   *
   * @return mixed|null the stored value, if any
   */
  public function lookup($key)
  {
    $_key = $key;

    $node = $this->root;
    while (true) {
      $keylen = strlen($key);

      foreach ($node as $prefix => $value) {
        if ($prefix === $key) {
          if ($value instanceof Node) {
            return $value->value;
          } else if (array_key_exists('', $value)) {
            return $value['']->value;
          }
          return null; // there's no actual match
        }

        $preflen = strlen($prefix);

        if ($preflen === 0) {
          // skip empty prefixes because if they didn't match above
          // they can't match below
          continue 1;
        }

        if ($keylen > $preflen &&
            substr($key, 0, $preflen) === $prefix) {
          // the prefix matches -- iterate further
          $key = substr($key, $preflen);
          $node = $value;
          continue 2;
        }
      }

      // we've went through all nodes and didn't find anything interesting
      // so there can be no further match
      return null;
    }
  }
}
