<?php declare(strict_types=1);

namespace PN\Questionnaire\Utilities;

/**
 * An object-oriented wrapper around the PHP array type.
 *
 * This class is not intended to be used directly; instead, subclasses can
 * override some or all of the logic pertaining to array manipulation.
 *
 * The iteration behaviour is undefined when modifying the underlying array
 * during the iteration.
 */
class Bag implements \ArrayAccess, \Iterator
{
  protected $bag, $iterator;

  /**
   * Creates a new `Bag`.
   *
   * @param array|null $initial The initial state of the Bag.
   */
  public function __construct($initial)
  {
    $this->bag = $initial;
  }

  /**
   * Return the contents of this Bag as a PHP array.
   *
   * A copy of the contents is returned, so modifications on the structure of
   * the returned array will not be reflected on the Bag. However, standard
   * rules for PHP by-reference or by-value apply, so objects are not cloned and
   * therefore their modifications will be visible in the Bag as well.
   *
   * @return array
   */
  public function toArray()
  {
    return $this->bag;
  }

  /** @internal */
  public function offsetExists($k) { return array_key_exists($k, $this->bag); }

  /** @internal */
  public function offsetGet($k) { return $this->bag[$k] ?? null; }

  /** @internal */
  public function offsetSet($k, $v) { $this->bag[$k] = $v; }

  /** @internal */
  public function offsetUnset($k) { unset($this->bag[$k]); }

  /** @internal */
  public function rewind()
  {
    $this->iterator = $this->createIterator()();
    return $this->iterator->rewind();
  }

  /** @internal */
  public function key() { return $this->iterator->key(); }

  /** @internal */
  public function current() { return $this->iterator->current(); }

  /** @internal */
  public function next() { return $this->iterator->next(); }

  /** @internal */
  public function valid() { return $this->iterator->valid(); }

  /**
   * Creates a `foreach` style iterator to be wrapped around by the Bag.
   */
  protected function createIterator()
  {
    return function() {
      foreach ($this->bag as $key => $value) {
        yield $key => $value;
      }
    };
  }
}
