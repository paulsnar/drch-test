<?php declare(strict_types=1);

namespace PN\Questionnaire;

use PN\Questionnaire\DB\{ConnectionFailure, PDOException};

/**
 * A thin wrapper around PDO for some common DB stuff.
 */
abstract class DB
{
  protected static $c;
  protected static function getHandle()
  {
    if (static::$c === null) {
      $db = Config::get('db');
      switch ($db['driver']) {
      case 'sqlite':
        try {
          $c = new \PDO("sqlite:{$db['file']}");
        } catch (\Throwable $err) {
          throw new ConnectionFailure($err);
        }
        break;

      case 'mysql':
        try {
          $c = new \PDO("mysql:host={$db['host']};dbname={$db['database']}",
            $db['username'], $db['password']);
        } catch (\Throwable $err) {
          throw new ConnectionFailure($err);
        }
        break;

      default:
        throw new \Exception("Unknown database driver: {$db['driver']}");
      }
      static::$c = $c;
    } else {
      $c = static::$c;
    }

    return $c;
  }

  /**
   * Execute `$callback` within a transaction.
   *
   * The $callback should return true when it wants the transaction to commit.
   * If it doesn't return a truthy value, the transaction will be rolled back.
   *
   * @param callable $callback
   */
  public static function transaction($callback)
  {
    $db = static::getHandle();

    $db->beginTransaction();
    $ok = false;

    try {
      $ok = $callback();
      if ($ok) {
        $db->commit();
      }
    } finally {
      if ( ! $ok) {
        $db->rollback();
      }
    }
  }

  // Stored for debugging purposes.
  protected static $last_query, $last_params;

  /** @internal */
  public static function getLastQuery()
  {
    return [ static::$last_query, static::$last_params ];
  }

  /**
   * Execute `$query` against the connection.
   *
   * `$query` can (and probably should) contain bind parameters, which are
   * supplied by `$params`.
   *
   * @param string $query
   * @param array $params
   *
   * @return \PDOStatement
   */
  public static function query($query, $params = [ ])
  {
    $db = static::getHandle();

    static::$last_query = $query;
    static::$last_params = $params;

    $q = $db->prepare($query);
    if ($q === false) {
      throw new PDOException($db->errorInfo());
    }

    foreach ($params as $key => $value) {
      if (is_integer($key)) {
        $key += 1;
      }
      $q->bindValue($key, $value);
    }

    $ok = $q->execute();
    if ( ! $ok) {
      throw new PDOException($q->errorInfo());
    }

    return $q;
  }

  /**
   * A convenience wrapper to fetch the first row from a given query.
   *
   * The same semantics apply as to `query`.
   *
   * @param string $query
   * @param array $params
   * @return array|null The row with associative column names.
   */
  public static function selectOne($query, $params = [ ])
  {
    $q = static::query($query, $params);
    $row = $q->fetch(\PDO::FETCH_ASSOC);
    if ($row === false) { return null; }
    return $row;
  }

  /**
   * A convenience wrapper to fetch all rows from a given query.
   *
   * Can be resource intensive -- you should probably use `query` and process
   * the rows iteratively.
   *
   * The same semantics apply as to `query`.
   *
   * @param string $query
   * @param array $params
   * @return array The rows, each of which has associative column names.
   */
  public static function selectAll($query, $params = [ ])
  {
    $q = static::query($query, $params);
    return $q->fetchAll(\PDO::FETCH_ASSOC);
  }
}
