<?php declare(strict_types=1);

namespace PN\Questionnaire;

/**
 * A short-term memory for values, presumably to be consumed at next request.
 */
abstract class Flash
{
  /**
   * Consume the value located at the given key, if any.
   *
   * @param string $name
   * @return mixed|null
   */
  public static function get($name)
  {
    $val = Session::get("internal.flash.{$name}");
    if ($val === null) {
      return null;
    }
    Session::unset("internal.flash.{$name}");
    return $val;
  }

  /**
   * Check for presence of a given key in the flash storage.
   *
   * @param string $name
   * @return bool
   */
  public static function has($name)
  {
    return Session::has("internal.flash.{$name}");
  }

  /**
   * Retrieve the value located at the given key, if any, and don't unset it.
   *
   * @param string $name
   * @return mixed|null
   */
  public static function peek($name)
  {
    return Session::get("internal.flash.{$name}");
  }

  /**
   * Set the given value at the specified key.
   *
   * Will remain in the session until consumed by `get` or `unset`.
   *
   * @param string $name
   * @param mixed $value
   */
  public static function set($name, $value)
  {
    return Session::set("internal.flash.{$name}", $value);
  }

  /**
   * Forget the value at the given key, if any.
   *
   * @param string $name
   */
  public static function unset($name)
  {
    return Session::unset("internal.flash.{$name}");
  }

  /**
   * Remove all values from the flash storage.
   */
  public static function clear()
  {
    return Session::unset('internal.flash');
  }
}
