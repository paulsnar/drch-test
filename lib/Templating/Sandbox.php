<?php declare(strict_types=1);

namespace PN\Questionnaire\Templating;

/**
 * The execution environment of templates.
 */
abstract class Sandbox
{
  const DEFAULT_PREAMBLE =
    'namespace PN\\Questionnaire\\Templating\\Stdlib;' .
    'use PN\\Questionnaire\\Flash;' .
    '?'.'>';

  /**
   * Evaluate the given PHP file code and return the result it generates.
   *
   * The code will be executed in context of the `Stdlib`. As an exception,
   * the `Flash` object will be available at top level.
   *
   * @param string $__code
   * @param array $__ctx
   * @return string
   */
  public static function eval(string $__code, array $__ctx = [ ])
  {
    Stdlib::ensureAutoloaded();

    extract($__ctx);
    ob_start();
    try {
      eval(static::DEFAULT_PREAMBLE . $__code);
      return ob_get_contents();
    } finally {
      ob_end_clean();
    }
  }
}
