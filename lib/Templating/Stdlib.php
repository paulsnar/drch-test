<?php declare(strict_types=1);

/*
  This entire file is somewhat a hack -- it lives in the class autoloading
  hierarchy, however it defines the namespace within which the templates run,
  therefore also the standard function library that's accessible to them.

  Don't take this the wrong way :)
*/

namespace PN\Questionnaire\Templating
{
  abstract class Stdlib {
    public static function ensureAutoloaded() { return true; }
  }
}

namespace PN\Questionnaire\Templating\Stdlib
{
  function he(string $msg) {
    return htmlspecialchars($msg, ENT_QUOTES | ENT_HTML5);
  }

  function json($val, $opts = 0) {
    $opts |= JSON_HEX_TAG;
    return htmlspecialchars(\json_encode($val, $opts), ENT_HTML5);
  }

  function url($u) {
    return \PN\Questionnaire\Web\Routing\Router::makeLink($u);
  }

  function csrf() {
    return \PN\Questionnaire\Web\CSRF::token();
  }
}
