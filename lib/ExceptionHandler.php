<?php declare(strict_types=1);

namespace PN\Questionnaire;

use PN\Questionnaire\DB\ConnectionFailure;

/**
 * The last-resort uncaught exception handler, as well as the bit which
 * converts errors into exceptions, because errors are...n't the best.
 */
abstract class ExceptionHandler
{
  /**
   * Register the error and exception handling.
   *
   * Should be called as part of the global initialization process.
   */
  public static function register()
  {
    error_reporting(\E_ALL);
    set_error_handler([ static::class, 'handleError' ]);
    set_exception_handler([ static::class, 'handleException' ]);
  }

  public static function handleException(\Throwable $err)
  {
    if (Config::$debug) {
      ob_end_clean();
      header('Content-Type: text/plain; charset=UTF-8');
      echo "--- Catastrophic Failure (lp0 on fire, is not a tty) ---\n";
      echo $err, PHP_EOL;
      [ $q, $params ] = DB::getLastQuery();
      if ($q !== null) {
        echo PHP_EOL, "Last executed DB query: {$q}\nParams: ";
        print_r($params);
        echo PHP_EOL;
      }
    } else {
      $cls = get_class($err);
      $file = $err->getFile();
      $line = $err->getLine();
      $msg = $err->getMessage();
      syslog(\LOG_ERR,
        "Unhandled {$cls} at {$file}:{$line}: {$msg}");
      static::sendFailure($err);
    }
    exit();
  }

  public static function handleError($severity, $msg, $file, $line)
  {
    throw new \ErrorException($msg, 0, $severity, $file, $line);
  }

  protected static function sendFailure($err)
  {
    ob_end_clean();
    http_response_code(500);
    header('Content-Type: text/html; charset=UTF-8');
    echo "<!DOCTYPE html>\n";
    echo "<article>\n";

    if ($err instanceof ConnectionFailure) {
      $failure = "Sorry, a database connection could not be established.";
    } else {
      $failure = "Sorry, something went wrong.";
    }

    echo "<p>{$failure}</p>\n";
    if ( ! Config::get('errors.disable_tips')) {
      echo "<p><em>Tip: enable <code>debug</code> in the configuration to ",
        "see more details.</em></p>";
    }
    echo "</article>\n";
  }
}
