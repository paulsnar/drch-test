<?php declare(strict_types=1);

namespace PN\Questionnaire;

/**
 * An implementation of an SPL-compatible autoloader for loading the library
 * and application code.
 */
abstract class Autoloader
{
  const NAMESPACE_LIBRARY = 'PN/Questionnaire/';
  const NAMESPACE_APPLICATION = 'PN/Questionnaire/App/';

  /**
   * Register this implementation of autoloading globally.
   *
   * Should be called as part of the global initialization.
   */
  public static function register()
  {
    spl_autoload_register([ static::class, 'autoload' ], true, true);
  }

  /**
   * Autoload the given class.
   *
   * @return bool whether the class loading was handled by this autoloader
   */
  public static function autoload($cls)
  {
    $file = str_replace('\\', '/', $cls);

    if (strpos($file, static::NAMESPACE_APPLICATION) === 0) {
      $file = str_replace(static::NAMESPACE_APPLICATION,
        Q_SOURCE_ROOT . '/', $file);
    } else if (strpos($file, 'PN/Questionnaire/') === 0) {
      $file = str_replace(static::NAMESPACE_LIBRARY,
        Q_LIBRARY_ROOT . '/', $file);
    } else {
      return false;
    }

    if (DIRECTORY_SEPARATOR !== '/') {
      $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
    }

    try {
      include "{$file}.php";
    } catch (\ErrorException $err) {
      throw new \Exception("Class not found: {$cls}", 0, $err);
    }
    return true;
  }
}
