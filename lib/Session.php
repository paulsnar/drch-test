<?php declare(strict_types=1);

namespace PN\Questionnaire;

/**
 * A thin wrapper around PHP's session storage facilities.
 *
 * Manages starting and clearing out the session before accessing it.
 *
 * Supports dot-style key notation.
 */
abstract class Session
{
  protected static $started = false;

  protected static function ensure()
  {
    if ( ! static::$started) {
      session_start();
      static::$started = true;
    }
  }

  /**
   * Access a value within the session.
   *
   * @param string $key
   * @return mixed|null
   */
  public static function get($key)
  {
    static::ensure();
    return Utilities::objNavigate($_SESSION, $key);
  }

  /**
   * Persist a given value in the session.
   *
   * @param string $key
   * @param mixed $value
   */
  public static function set($key, $value)
  {
    static::ensure();
    return Utilities::objNavigateSet($_SESSION, $key, $value);
  }

  /**
   * Remove a given value from the session.
   *
   * @param string $key
   */
  public static function unset($key)
  {
    static::ensure();
    return Utilities::objNavigateUnset($_SESSION, $key);
  }

  /**
   * Check whether a given key is set in the session.
   *
   * @param string $key
   * @return bool
   */
  public static function has($key)
  {
    static::ensure();
    return Utilities::objNavigateHas($_SESSION, $key);
  }

  /**
   * Destroy the session, forgetting all values.
   *
   * Will start the session if it isn't already, so can be needlessly wasteful.
   */
  public static function destroy()
  {
    static::ensure();
    session_unset();
    session_destroy();
    session_write_close();
    static::$started = false;
  }
}
