<?php declare(strict_types=1);

namespace PN\Questionnaire;

abstract class Utilities
{
  /**
   * Locate a value within an object, using a dot-style key.
   *
   * @param array $obj
   * @param string $key The dotted key (e.g. 'a.b.c.')
   *
   * @return mixed|null
   */
  public static function objNavigate($obj, $key)
  {
    $key = explode('.', $key);
    $leaf = array_pop($key);
    foreach ($key as $segment) {
      if ( ! array_key_exists($segment, $obj)) {
        return null;
      }
      $obj = $obj[$segment];
    }
    return $obj[$leaf] ?? null;
  }

  /**
   * Check whether a dot-style key corresponds to a value within an object.
   *
   * @param array $obj
   * @param string $key
   *
   * @return bool
   */
  public static function objNavigateHas($obj, $key)
  {
    $key = explode('.', $key);
    $leaf = array_pop($key);
    foreach ($key as $segment) {
      if ( ! array_key_exists($segment, $obj)) {
        return false;
      }
      $obj = $obj[$segment];
    }
    return array_key_exists($leaf, $obj);
  }

  /**
   * Set a value within an object, using a dot-style key.
   *
   * Creates the intermediate objects as necessary. Will overwrite
   * intermediate values that are not arrays.
   *
   * @param array $obj
   * @param string $key The dotted key (e.g. 'a.b.c')
   * @param mixed $value
   */
  public static function objNavigateSet(&$obj, $key, $value)
  {
    $key = explode('.', $key);
    $leaf = array_pop($key);
    foreach ($key as $segment) {
      if ( ! array_key_exists($segment, $obj) || ! is_array($obj[$segment])) {
        $obj[$segment] = [ ];
      }
      $obj =& $obj[$segment];
    }
    $obj[$leaf] = $value;
  }

  /**
   * Unset a dot-style key within an object.
   *
   * @param array $obj
   * @param string $key
   */
  public static function objNavigateUnset(&$obj, $key)
  {
    $key = explode('.', $key);
    $leaf = array_pop($key);
    foreach ($key as $segment) {
      if ( ! array_key_exists($segment, $obj)) {
        return;
      }
      $obj =& $obj[$segment];
    }
    unset($obj[$leaf]);
  }

  /**
   * A recursive deep merge which overwrites arrays instead of
   * appending to them.
   *
   * @return array
   */
  public static function arrayOverride(array $base, ...$arrays)
  {
    foreach ($arrays as $array) {
      foreach ($array as $key => $value) {
        if (is_array($value)) {
          if (array_key_exists($key, $base)) {
            $base[$key] = static::arrayOverride($base[$key], $value);
          } else {
            $base[$key] = $value;
          }
        } else {
          $base[$key] = $value;
        }
      }
    }

    return $base;
  }

  /**
   * Dump the given value to stderr.
   *
   * Useful for debugging, maybe.
   */
  public static function dump($val)
  {
    file_put_contents('php://stderr', print_r($val, true) . PHP_EOL);
  }
}
