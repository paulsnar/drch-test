<?php declare(strict_types=1);

namespace PN\Questionnaire\Web;

use PN\Questionnaire\Session;

/**
 * CSRF protection.
 */
abstract class CSRF
{
  protected static $generated;

  protected static function generateToken()
  {
    static::$generated = true;
    return bin2hex(random_bytes(12));
  }

  /**
   * Create a new CSRF token to be sent to the client and used in the next
   * request.
   *
   * The token is stored in the session and is valid only for one request.
   * Cookies must be enabled for this to work.
   *
   * @return string the token to be sent
   */
  public static function token()
  {
    $tok = static::generateToken();
    Session::set('internal.csrf', $tok);
    return $tok;
  }

  /**
   * Check the provided token.
   *
   * The stored token will be discarded in the verification process, regardless
   * of the validity of the submitted one. For the next request a new one must
   * be generated.
   *
   * @return bool
   */
  public static function verify(string $offer)
  {
    $actual = Session::get('internal.csrf');
    if ($actual === null) {
      return false;
    }
    Session::unset('internal.csrf');

    return hash_equals($actual, $offer);
  }

  /**
   * Throws away the current CSRF token.
   *
   * A new one must be generated for the next request.
   */
  public static function discard()
  {
    Session::unset('internal.csrf');
  }
}
