<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\Routing;

use PN\Questionnaire\Config;
use PN\Questionnaire\Web\HTTP\Request;
use PN\Questionnaire\Utilities\{PrefixTree, RadixTree};

abstract class Router
{
  protected static $routes, $exact;

  // This cannot be expressed statically, so we do it lazily dynamically!
  protected static function createRoutes()
  {
    static::$routes = new PrefixTree([
      '' => new DefaultNotFoundRoute(),
    ]);

    static::$exact = new RadixTree();
  }

  public static function register(Route $route)
  {
    if (static::$routes === null) {
      static::createRoutes();
    }

    if ($route->exact) {
      static::$exact->insert($route->path, $route);
    } else {
      static::$routes->insert($route->path, $route);
    }
  }

  public static function dispatch(Request $rq)
  {
    if (static::$routes === null) {
      static::createRoutes();
    }

    $exact = static::$exact->lookup($rq->path);
    if ($exact !== null) {
      return $exact;
    }

    return static::$routes->lookup($rq->path);
  }

  public static function makeLink($path)
  {
    $prefix = Config::get('routing.prefix');
    if ($prefix === null) {
      $prefix = '';
    }
    return rtrim($prefix, '/') . '/' . ltrim($path, '/');
  }
}
