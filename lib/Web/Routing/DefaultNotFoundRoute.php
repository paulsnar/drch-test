<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\Routing;

use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\HTTP\{Request, Response};

class DefaultNotFoundRoute extends Route
{
  const DEFAULT_ERROR_MESSAGE = "<!DOCTYPE html>\n" .
    "<article>Sorry, not found.</article>\n";

  public const PATH = ''; // mounted at the root of the prefix tree

  public function any(Request $rq)
  {
    return new Response(HTTP::NOT_FOUND, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ], static::DEFAULT_ERROR_MESSAGE);
  }

  public function get(Request $rq)
  {
    return $this->any($rq);
  }
}
