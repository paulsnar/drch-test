<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\Routing;

use PN\Questionnaire\Web;
use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\HTTP\{Request, Response};

/**
 * A set of response generators for a given path.
 *
 * Every method on the route corresponds to an HTTP method. You can define
 * your custom ones as well if you want (hello, WebDAV).
 *
 * GET must be implemented by all routes. The rest are optional and by default
 * return a 405 (Method Not Allowed).
 *
 * A special method `any` can be defined, to which all of the requests will
 * be routed, regardless of their request method.
 */
abstract class Route
{
  /** @var string where to mount this route */
  public $path;

  /** @var bool whether to match all subtrees or only the exact $path */
  public $exact = true;

  // Every route must implement GET.
  abstract public function get(Request $rq);

  // Not every route must implement HEAD though, and we can derive it from GET.
  public function head(Request $rq)
  {
    return $this->get($rq);
  }

  // The other methods have a default implementation which just yells
  // 405 at the requester.

  public function options(Request $rq)
  {
    return new Response(HTTP::METHOD_NOT_ALLOWED, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ], Web::DEFAULT_HTML_ERROR_MESSAGE);
  }

  public function post(Request $rq)
  {
    return new Response(HTTP::METHOD_NOT_ALLOWED, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ], Web::DEFAULT_HTML_ERROR_MESSAGE);
  }

  public function put(Request $rq)
  {
    return new Response(HTTP::METHOD_NOT_ALLOWED, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ], Web::DEFAULT_HTML_ERROR_MESSAGE);
  }

  public function delete(Request $rq)
  {
    return new Response(HTTP::METHOD_NOT_ALLOWED, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ], Web::DEFAULT_HTML_ERROR_MESSAGE);
  }
}
