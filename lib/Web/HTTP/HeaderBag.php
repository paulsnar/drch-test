<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\HTTP;

use PN\Questionnaire\Utilities\Bag;

/**
 * A case-insensitive Bag, useful for storing e.g. HTTP headers.
 */
class HeaderBag extends Bag
{
  public function __construct($headers)
  {
    $hds = [ ];
    foreach ($headers as $name => $value) {
      $hds[strtolower($name)] = $value;
    }
    parent::__construct($hds);
  }

  public function offsetExists($k)
  {
    return parent::offsetExists(strtolower($k));
  }

  public function offsetGet($k)
  {
    return parent::offsetGet(strtolower($k));
  }

  public function offsetSet($k, $v)
  {
    return parent::offsetSet(strtolower($k), $v);
  }

  public function offsetUnset($k)
  {
    return parent::offsetUnset(strtolower($k));
  }

  /**
   * Creates a new HeaderBag from the current request state.
   */
  public static function fromGlobals()
  {
    $hds = [ ];
    foreach ($_SERVER as $key => $value) {
      if (strpos($key, 'HTTP_') === 0) {
        $hd_name = substr($key, 5);
        $hd_name = str_replace('_', '-', $hd_name);
        $hds[$hd_name] = $value;
      }
    }
    return new static($hds);
  }
}
