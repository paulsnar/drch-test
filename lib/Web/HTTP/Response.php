<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\HTTP;

use PN\Questionnaire\Template;
use PN\Questionnaire\Web\HTTP;
use PN\Questionnaire\Web\Routing\Router;

/**
 * An HTTP response to an incoming request.
 */
class Response
{
  public $status;
  public $headers;
  public $cookies = [ ];
  public $body;

  /**
   * Creates a new Response.
   *
   * @param int $status
   * @param array $headers
   * @param string|null body
   */
  public function __construct($status = 204, $headers = [ ], $body = null)
  {
    $this->status = $status;
    $this->headers = new HeaderBag($headers);
    $this->body = $body;
  }

  /**
   * Sends the Response.
   *
   * Modifies global request/response state.
   */
  public function send()
  {
    http_response_code($this->status);

    foreach ($this->headers as $header => $value) {
      if ($value === null) {
        header_remove($header);
      } else {
        header("{$header}: {$value}");
      }
    }

    foreach ($this->cookies as $cookie) {
      $cookie->send();
    }

    if ($this->body !== null) {
      echo $this->body;
    }

    if (function_exists('fastcgi_finish_request')) {
      fastcgi_finish_request();
    }
  }

  /**
   * Create a Response, using defaults and information from the Template.
   *
   * @param Template $template
   * @return Response
   */
  public static function fromTemplate($status, Template $tpl)
  {
    $content = $tpl->render();
    $headers = [ ];

    if (array_key_exists('http_headers', $tpl->metadata)) {
      foreach ($tpl->metadata['http_headers'] as $name => $value) {
        $name = str_replace('_', '-', $name);
        $headers[$name] = $value;
      }
    }

    $headers['Content-Type'] =
      $tpl->metadata['mediatype'] ?? 'text/html; charset=UTF-8';

    return new static($status, $headers, $content);
  }

  /**
   * Create a Response that 302 redirects to the given URL.
   *
   * @param string $target
   * @param int $status
   */
  public static function redirectTo($target, $status = HTTP::FOUND)
  {
    if ($target[0] === '/' && strpos($target, '//') !== 0) {
      // semi-absolute URL
      $target = Router::makeLink($target);
    }
    return new static($status, [
      'Content-Type' => 'text/html; charset=UTF-8',
      'Location' => $target,
    ], "<!DOCTYPE html>\n" .
      '<article>You are being <a href="' .
      htmlspecialchars($target, \ENT_QUOTES | \ENT_HTML5) .
      '">redirected</a>.</article>' . "\n");
  }

  /**
   * Create a Response that contains the given JSON object as its body.
   *
   * @param int $status
   * @param mixed $value
   */
  public static function withJson($status, $value)
  {
    return new static($status, [
      'Content-Type' => 'application/json; charset=UTF-8',
    ], json_encode($value, JSON_UNESCAPED_SLASHES));
  }
}
