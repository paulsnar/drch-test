<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\HTTP;

use PN\Questionnaire\Utilities\Bag;

/**
 * A bag which stores values which might be either scalars or arrays,
 * and by default returns the first value of an array.
 */
class ValueBag extends Bag
{
  /** @inheritdoc */
  public function offsetGet($k)
  {
    $v = parent::offsetGet($k);
    if (is_array($v)) {
      $v = $v[0];
    }
    return $v;
  }

  /**
   * Return all the values provided for a key in array form.
   *
   * If only one value was provided, it will be wrapped in an array.
   *
   * @return array
   */
  public function all($k)
  {
    $v = parent::offsetGet($k);
    if ( ! is_array($v)) {
      $v = [ $v ];
    }
    return $v;
  }
}
