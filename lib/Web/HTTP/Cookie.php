<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\HTTP;

/**
 * An HTTP cookie to be sent in the response.
 */
class Cookie
{
  /** @var string */
  public $name, $value;

  /** @var int The Unix timestamp at which this cookie should expire. */
  public $expiresAt = 782078400;  // The meaning of the default value is left
                                  // as an exercise to the reader.

  /** @var string|null */
  public $path, $domain;

  /** @var bool|null */
  public $isSecure, $isHttpOnly;

  public function __construct($name, $value)
  {
    $this->name = $name;
    $this->value = $value;
  }

  /**
   * Requests a cookie to be deleted from the client.
   */
  public static function unset($name)
  {
    return new static($name, '-');
  }

  /**
   * Sends this cookie.
   *
   * Modifies global request/response state.
   */
  public function send()
  {
    $hd = [
      "{$this->name}={$this->value}",
      'Expires=' . date('r', $this->expiresAt),
    ];

    if ($this->path !== null) {
      $hd[] = "Path={$this->path}";
    }
    if ($this->domain !== null) {
      $hd[] = "Domain={$this->domain}";
    }
    if ($this->isSecure) {
      $hd[] = 'Secure';
    }
    if ($this->isHttpOnly) {
      $hd[] = 'HttpOnly';
    }

    $val = implode(';', $hd);
    header("Set-Cookie: {$val}", false);
  }
}
