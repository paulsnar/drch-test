<?php declare(strict_types=1);

namespace PN\Questionnaire\Web\HTTP;

use PN\Questionnaire\Config;
use PN\Questionnaire\Utilities\Bag;

/**
 * An object-oriented wrapper around an HTTP request.
 */
class Request
{
  public $method;
  public $path;
  public $headers;
  public $query;
  public $form;
  public $files;
  public $cookies;

  /**
   * Creates a new Request from the global HTTP request state.
   */
  public static function fromGlobals()
  {
    $rq = new static();

    $rq->method = $_SERVER['REQUEST_METHOD'];

    $path = $_SERVER['REQUEST_URI'];

    if (strpos($path, '?') !== false) {
      $query_str = substr($path, strpos($path, '?') + 1);
      parse_str($query_str, $query);
      $path = substr($path, 0, strpos($path, '?'));
    } else {
      $query = $_GET;
    }

    if (Config::has('routing.prefix')) {
      $prefix = Config::get('routing.prefix');
      $prefix = rtrim($prefix, '/') . '/';
      if (strpos($path, $prefix) === 0) {
        $path = '/' . substr($path, strlen($prefix));
      }
    }

    $rq->path = $path;

    $rq->headers = HeaderBag::fromGlobals();
    $rq->query = new ValueBag($query);
    $rq->form = new ValueBag($_POST);
    $rq->files = new ValueBag($_FILES);
    $rq->cookies = new ValueBag($_COOKIE);

    return $rq;
  }
}
