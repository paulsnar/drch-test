<?php declare(strict_types=1);

namespace PN\Questionnaire;

use PN\Questionnaire\Templating\Sandbox;

const Q_TEMPLATE_ROOT = Q_PRIVATE_ROOT . DIRECTORY_SEPARATOR . 'tpl';

/**
 * A template to be rendered and displayed.
 *
 * The context can be accessed by using the template as an associative array.
 *
 * Templates should reside in the directory defined by Q_TEMPLATE_ROOT
 * (normally - the `tpl` subdirectory of the project root), and have a
 * `.tpl.php` extension. They can be organized into directories. The template
 * name used in the constructor is the template filename relative to the
 * template root, where subdirectories are denoted with a dot (.) instead of a
 * slash (/) -- so `layouts/common.tpl.php` becomes `layouts.common`.
 *
 * Template content can be prefixed by the so-called "preamble", which is just
 * an INI text contained within triple dashes (`---\n`).
 *
 * The contents of the preamble can be flexible, and the `$metadata` property
 * will be filled out by them, but some conventions have been defined:
 *
 * * `layout` will define the layout from which this template inherits.
 *    This template will be rendered, and then the layout will be rendered with
 *    the current template content available as `$content`.  
 *   `vars` is an associative array which provides additional context for the
 *    layout template, as well as defaults for the current template.
 * * `mediatype` will be interpreted by the response, setting the Content-Type
 *    HTTP header.
 * * `http_headers` can contain HTTP headers to be set on the response. Note
 *    that the INI format doesn't like dashes, so use underscores instead.
 */
class Template implements \ArrayAccess
{
  public static $globals = [ ];

  public $name, $env, $layout, $content, $metadata = [ ];
  protected $path;

  /**
   * Create a new instance of Template.
   *
   * @param string $name The dotted name of the template.
   */
  public function __construct($name)
  {
    $this->name = $name;
    $this->env = static::$globals; // implicit clone

    $fsname = str_replace('.', DIRECTORY_SEPARATOR, $name);
    $this->path = path_join(Q_TEMPLATE_ROOT, "{$fsname}.tpl.php");
  }

  /** @internal */
  public function offsetExists($k) { return array_key_exists($k, $this->env); }
  public function offsetGet($k) { return $this->env[$k] ?? null; }
  public function offsetSet($k, $v) { $this->env[$k] = $v; }
  public function offsetUnset($k) { unset($this->env[$k]); }

  /**
   * Renders the given template with the given context.
   *
   * If the template has specified a layout, it will be rendered too, however
   * the internal `layout` property should not be modified beforehand as it
   * will be overwritten.
   *
   * The result of the rendering will be cached and returned. If you want to
   * render the template anew, reset `$content` to null.
   *
   * @return string the rendered template's body
   */
  public function render()
  {
    // template has already been rendered
    if ($this->content !== null) {
      if ($this->layout !== null) {
        return $this->layout->content;
      }
      return $this->content;
    }

    try {
      $content = file_get_contents($this->path);
    } catch (\ErrorException $err) {
      throw new \Exception("Template {$this->name} not found.", 0, $err);
    }

    if (strpos($content, '---') === 0) {
      $preamble_end = strpos($content, "\n---\n", 3);
      if ($preamble_end === false) {
        goto skip_preamble;
      }

      $preamble = substr($content, 3, $preamble_end - 3);
      $content = substr($content, $preamble_end + 4);

      $this->metadata = parse_ini_string($preamble, true, INI_SCANNER_TYPED);
    }

    $vars = $this->metadata['vars'] ?? [ ];
    $this->env = Utilities::arrayOverride($vars, $this->env);

  skip_preamble:
    $content = $this->content = Sandbox::eval($content, $this->env);

    $layout = $this->metadata['layout'] ?? null;
    if ($layout !== null) {
      $layout = $this->layout = new Template($layout);
      $layout->env['content'] = $content;

      $layout->env = Utilities::arrayOverride($layout->env, $vars);

      $content = $layout->render();
      $this->metadata = Utilities::arrayOverride(
        $layout->metadata, $this->metadata);
      if (array_key_exists('mediatype', $layout->metadata)) {
        $this->metadata['mediatype'] = $layout->metadata['mediatype'];
      }
    }

    return $content;
  }
}
