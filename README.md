# Draugiem Group vasaras programmēšanas skolas kvalifikācijas uzdevums

Šajā repozitorijā atrodama [uzdevumā][task-spec] specifizētā testu uzdošanas
tīmekļlietotne.

Dokumentācija pieejama [šajā mapē](./docs).

[task-spec]: https://docs.google.com/document/d/1v9itvDhiHop0wlhJgHMVcneDdcC7d33yDyCtg5Tq-Cg/edit?usp=sharing

## Licence

[BSD 3-pantu](./LICENSE.txt)
