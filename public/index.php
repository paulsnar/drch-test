<?php declare(strict_types=1);

namespace PN\Questionnaire;

// Passthrough for static files in development mode
if (\PHP_SAPI === 'cli-server') {
  if (is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'])['path'])) {
    return false;
  }
}

function path_join(...$parts) {
  return implode(DIRECTORY_SEPARATOR, $parts);
}

define('Q_PUBLIC_ROOT', __DIR__);
define('Q_PRIVATE_ROOT', dirname(__DIR__));
define('Q_LIBRARY_ROOT', path_join(Q_PRIVATE_ROOT, 'lib'));
define('Q_SOURCE_ROOT', path_join(Q_PRIVATE_ROOT, 'src'));

require path_join(Q_LIBRARY_ROOT, 'Autoloader.php');

Autoloader::register();
ExceptionHandler::register();
Config::load();

Web::dispatch();
